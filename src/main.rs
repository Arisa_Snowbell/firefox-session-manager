use std::{
	collections::HashMap,
	ffi::OsString,
	fmt::Display,
	fs::{self, DirBuilder, OpenOptions},
	io::Read,
	path::{Path, PathBuf},
	process::exit,
	str::FromStr,
};

use anyhow::{bail, Result};
use chrono::{DateTime, Local, NaiveDateTime};
use clap::{crate_authors, crate_description, crate_version, App, AppSettings, Arg, SubCommand};
use colored::*;
use dialoguer::Select;
use encoding_rs_io::DecodeReaderBytes;
use ini::Ini;
use thiserror::Error;
use walkdir::WalkDir;

#[derive(Debug, strum_macros::Display)]
enum Which {
	Latest,
	Previous,
	Other,
}

#[derive(Error, Debug)]
enum Error {
	#[error("Couldn't parse &str to enum Which.")]
	ParseErrorForWhich,
	#[error("Couldn't find current session.")]
	MissingCurrentSession,
}

impl FromStr for Which {
	type Err = Error;

	fn from_str(s: &str) -> Result<Self, Self::Err> {
		Ok(match s.to_lowercase().as_ref() {
			"latest" | "current" | "last" | "curr" => Which::Latest,
			"previous" | "prev" => Which::Previous,
			"other" => Which::Other,
			_ => return Err(Error::ParseErrorForWhich),
		})
	}
}

const BAK_EXT: &str = "baklz4";
const JSON_EXT: &str = "jsonlz4";

struct Entry {
	path: PathBuf,
	datetime: NaiveDateTime,
}

impl Display for Entry {
	fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
		write!(f, "{}", self.path.file_stem().unwrap().to_str().unwrap())
	}
}

fn main() -> Result<()> {
	let app = App::new("Firefox Session Manager")
		.global_settings(&[
			AppSettings::GlobalVersion,
			AppSettings::ColorAuto,
			AppSettings::ColoredHelp,
			AppSettings::VersionlessSubcommands,
			AppSettings::InferSubcommands,
		])
		.setting(AppSettings::SubcommandRequiredElseHelp)
		.version(crate_version!())
		.author(crate_authors!(" & "))
		.about(crate_description!())
		.bin_name("fsm")
		.args(&[
			Arg::with_name("FIREFOX_FOLDER")
				.short("f")
				.long("fpath")
				.value_name("FIREFOX_FOLDER")
				.help("Override the path to Firefox folder aka working directory")
				.takes_value(true)
				.global(true),
			Arg::with_name("PROFILE")
				.short("P")
				.long("profile")
				.value_name("PROFILE")
				.help("Override which profile to use")
				.takes_value(true)
				.global(true),
			Arg::with_name("STORAGE_FOLDER")
				.long("sfolder")
				.short("S")
				.value_name("STORAGE_FOLDER")
				.help("Override path to search in folder")
				.takes_value(true)
				.global(true),
		])
		.subcommands(vec![
			SubCommand::with_name("backup").alias("bak").about("Backup current session"),
			SubCommand::with_name("load").about("Load session from storage").args(&[
				Arg::with_name("WHICH")
					.short("w")
					.long("which")
					.help("Which state of session")
					.value_name("WHICH")
					.takes_value(true)
					.required_unless("SESSION_FILE")
					.possible_values(&["latest", "last", "previous", "prev", "current", "curr", "other"])
					.case_insensitive(true)
					.index(1),
				Arg::with_name("SESSION_FILE")
					.long("sfile")
					.short("s")
					.value_name("SESSION_FILE")
					.help("Override path to load from")
					.takes_value(true)
					.required_unless("WHICH"),
			]),
			SubCommand::with_name("setup").about("Setup default settings"),
		])
		.get_matches();

	let default_storage_folder = dirs::document_dir().expect("Couldn't get document folder.").join("fsm");
	let config_folder = dirs::config_dir().expect("Couldn't get config folder.").join("fsm");
	let storage_folder = match app.value_of("STORAGE_FOLDER") {
		Some(v) => PathBuf::from_str(v)?,
		None => default_storage_folder.clone(),
	};
	let storage_backup_folder = default_storage_folder.join("backup");
	let config_file = config_folder.join("config").with_extension("ini");

	if !config_folder.exists() {
		DirBuilder::new().recursive(true).create(&config_folder)?;
	}

	if !storage_folder.exists() {
		DirBuilder::new().recursive(true).create(&storage_folder)?;
	}

	if !storage_backup_folder.exists() {
		DirBuilder::new().recursive(true).create(&storage_backup_folder)?;
	}

	let firefox_folder = match app.value_of("FIREFOX_FOLDER") {
		Some(path) => PathBuf::from_str(path)?,
		#[cfg(all(target_family = "unix", not(target_os = "macos")))]
		None => dirs::home_dir().expect("Couldn't get the Home directory.").join(".mozilla").join("firefox"),
		#[cfg(any(target_family = "windows", target_os = "macos"))]
		None => dirs::data_dir().expect("Couldn't get AppData directory.").join("Mozilla").join("Firefox"),
	};

	if !firefox_folder.exists() {
		println!("Firefox is very likely not installed on this machine, or was never run on this machine before.");
		exit(4);
	}

	let profile_file = firefox_folder.join("profiles").with_extension("ini");
	let _installs_file = firefox_folder.join("installs").with_extension("ini");

	let profiles = match load_ini_file(&profile_file) {
		Ok(v) => v,
		Err(e) => panic!("Couldn't parse the profiles.ini file: because {}.", e),
	};

	let mut paths: HashMap<String, String> = HashMap::with_capacity(1);

	for (section_name, section_properties) in profiles.iter() {
		for (value_name, value) in section_properties.iter() {
			if &value_name.to_lowercase() == "path" {
				paths.insert(
					match section_name {
						Some(v) => v.to_string(),
						None => format!("Profile{}", paths.len()),
					},
					value.to_string(),
				);
			}
		}
	}

	if paths.is_empty() {
		println!("Couldn't find any profiles in your Firefox profiles.ini.");
		exit(5);
	}

	let config = {
		if config_file.exists() {
			match load_ini_file(&config_file) {
				Ok(v) => v,
				Err(e) => panic!("Couldn't parse the profiles.ini file: because {}.", e),
			}
		} else {
			setup(paths.keys().cloned().collect::<Vec<String>>(), &config_file)?
		}
	};

	if app.subcommand_matches("setup").is_some() {
		setup(paths.keys().cloned().collect::<Vec<String>>(), &config_file)?;
		exit(0);
	}

	let profile = match app.value_of("PROFILE") {
		Some(v) => v,
		None => match config.get_from::<String>(None, "profile") {
			Some(v) => v,
			None => {
				println!("Run setup.");
				exit(8);
			}
		},
	};
	let curr_profile = paths.get(profile).unwrap_or_else(|| {
		if app.is_present("PROFILE") {
			println!("The provided profile named \"{}\" doesn't exist.", profile.red());
		} else {
			println!("Profile named \"{}\" doesn't exist. Probably run setup, to change default profile.", profile.red());
		}
		exit(7);
	});
	let profile_folder = firefox_folder.join(curr_profile);

	match app.subcommand() {
		("backup", Some(_)) => {
			backup_curr_session(&profile_folder, &storage_folder)?;
		}
		("load", Some(sub_p)) => {
			let which = Which::from_str(sub_p.value_of("WHICH").unwrap_or("other"))?;
			println!("Loading up the {} session.", &which);

			let fpath = match sub_p.value_of("SESSION_FILE") {
				Some(v) => PathBuf::from_str(v)?,
				None => {
					let mut list: Vec<Entry> = Vec::new();

					for entry in WalkDir::new(&storage_folder).max_depth(1) {
						let entry = entry?;
						let p = entry.path();

						if p.exists() && p.is_file() && p.extension() == Some(&OsString::from(JSON_EXT)) {
							let name = p.file_stem().unwrap().to_str().unwrap();
							let datetime = NaiveDateTime::parse_from_str(name, "%F_%H-%M-%S")?;
							list.push(Entry { path: p.to_path_buf(), datetime })
						}
					}

					list.sort_by(|Entry { path: _, datetime: a_time }, Entry { path: _, datetime: b_time }| b_time.cmp(a_time));

					match which {
						Which::Latest => match list.get(0) {
							Some(Entry { path, datetime: _ }) => path.clone(),
							None => {
								println!("Couldn't find the latest session file in storage folder.");
								exit(10);
							}
						},
						Which::Previous => match list.get(1) {
							Some(Entry { path, datetime: _ }) => path.clone(),
							None => {
								println!("Couldn't find the previous session file in storage folder.");
								exit(10);
							}
						},
						Which::Other => {
							if list.is_empty() {
								println!("There are no session files in the storage folder.");
								exit(43);
							}

							let selection = Select::new()
								.with_prompt("Which session file to load?")
								.default(0)
								.items(&list[..])
								.paged(true, Some(2))
								.clear(true)
								.interact_opt()?;

							if let Some(selection) = selection {
								list[selection].path.clone()
							} else {
								println!("You didn't choose any session file.");
								exit(76);
							}
						}
					}
				}
			};

			if !&fpath.exists() {
				if sub_p.is_present("SESSION_FILE") {
					println!("The provided path to session file is not valid.");
				} else {
					println!("The found path to session file is not valid.");
				}
				exit(9);
			}

			backup_curr_session(&profile_folder, &storage_backup_folder)?;

			let name = &fpath.file_stem().unwrap().to_str().unwrap();
			let datetime = NaiveDateTime::parse_from_str(name, "%F_%H-%M-%S")?;
			fs::copy(fpath, get_current_session_path(&profile_folder)?)?;
			println!("Finished loading up the {} session. Session Date: {}", &which, datetime.format("%F %X"));
		}
		_ => {
			println!("Either no subcommand entered or nobody made case for it.\nLook at help with command \"help\".")
		} // Either no subcommand or one not tested for...
	}

	Ok(())
}

fn backup_curr_session(profile_folder: &Path, storage_folder: &Path) -> Result<()> {
	println!("Backing up the current session.");
	let sessionstore_path = get_current_session_path(profile_folder)?;
	let datetime: DateTime<Local> = fs::metadata(&sessionstore_path)?.modified()?.into();
	let name = format!("{}", datetime.format("%F_%H-%M-%S")); // ISO 8601
	fs::copy(&sessionstore_path, &storage_folder.join(name).with_extension(JSON_EXT))?;
	println!("Finished backing up the current session. Session Date: {}", datetime.format("%F %X"));

	Ok(())
}

fn load_ini_file(profile_file: &Path) -> Result<Ini> {
	let mut profile_file = OpenOptions::new().read(true).open(profile_file)?;
	let mut contents = Vec::new();
	profile_file.read_to_end(&mut contents)?;
	let mut decoder = DecodeReaderBytes::new(&*contents);
	let mut contents = String::new();
	decoder.read_to_string(&mut contents)?;

	Ok(Ini::load_from_str(&contents)?)
}

fn get_current_session_path(path: &Path) -> Result<PathBuf> {
	let main = path.join("sessionstore").with_extension(JSON_EXT);
	if main.exists() {
		return Ok(main);
	}
	// Browser is probably running or were never ran before on this machine!

	let recovery = path.join("sessionstore-backups").join("recovery").with_extension(JSON_EXT);
	if recovery.exists() {
		return Ok(recovery);
	}
	// Very likely browser were not ran on this machine before but still try to get backup copy of recovery session store

	let recovery_bak = path.join("sessionstore-backups").join("recovery").with_extension(BAK_EXT);
	if recovery_bak.exists() {
		return Ok(recovery_bak);
	}

	bail!(Error::MissingCurrentSession)
}

fn setup(profiles: Vec<String>, path: &Path) -> Result<Ini> {
	let mut ini = match path.exists() {
		true => load_ini_file(path)?,
		false => Ini::new(),
	};

	let selection = Select::new()
		.with_prompt("What is your profile you want as default to be used?")
		.default(0)
		.items(&profiles[..])
		.paged(true, Some(2))
		.clear(true)
		.interact_opt()?;

	let profile = if let Some(selection) = selection {
		profiles[selection].clone()
	} else {
		println!("You didn't select anything.");
		exit(76);
	};

	ini.set_to::<String>(None, String::from("profile"), profile);

	ini.write_to_file(path)?;

	Ok(ini)
}
