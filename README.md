<a href="https://repology.org/project/firefox-session-manager/versions">
    <img src="https://repology.org/badge/vertical-allrepos/firefox-session-manager.svg" alt="Packaging Status" align="right">
</a>

[![pipeline status](https://gitlab.com/Arisa_Snowbell/firefox-session-manager/badges/domina/pipeline.svg)](https://gitlab.com/Arisa_Snowbell/firefox-session-manager/-/commits/domina)
[![dependency status](https://deps.rs/repo/gitlab/arisa_snowbell/firefox-session-manager/status.svg)](https://deps.rs/repo/gitlab/arisa_snowbell/firefox-session-manager)
[![](https://tokei.rs/b1/gitlab/Arisa_Snowbell/firefox-session-manager)](https://gitlab.com/Arisa_Snowbell/firefox-session-manager)

Firefox Session Manager
=======================


Warning!
========

Your file system needs to support modified time on files! for now at least...


Description
-----------

A CLI program to manage your Firefox sessions!

Downloads
---------

**Releases** - The binary can be downloaded at [Release Page](https://gitlab.com/Arisa_Snowbell/firefox-session-manager/-/releases).

**Source** - The source can be downloaded as a [.compressed archive](https://gitlab.com/Arisa_Snowbell/firefox-session-manager/-/archive/domina/firefox-session-manager-domina.zip), or cloned from our [GitLab Repo](https://gitlab.com/Arisa_Snowbell/firefox-session-manager).


"Manual" Installation (Not Finished)
---------------------
(If possible use your favorite package manager!)

Requirements:
- rustup with updated nightly toolchain


Git clone or download though other means from repo [^Downloads](#Downloads)

**Linux**

- Go to a folder named "installer"
- Run with sudo file named "install.sh"

**Darwin**

TODO: Add Darwin installation process

**Windows**

TODO: Add Windows installation process


Project Members
---------------

- [Arisa](https://gitlab.com/Arisa_Snowbell)
- [Ria](https://gitlab.com/)


License
-------
The license is `GNU General Public License v3.0`
